
variable "b_vpc_cidr" {
    type= "string"
    default= "10.0.0.0/16"
    description = "grp b vpc cidr"
}

variable "b_pubsubnet_cidr" {
    type= "string"
    default= "10.0.0.0/24"
    description = "grp b public subnet cidr"
}

variable "b_pubsubnet_name" {
    type= "string"
    default = "groub b pubsubnet"
    description= " b publc subnet name"
  
}